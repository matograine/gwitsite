# Je suis un voyou
G.Brassens

Capo 2
```
G      G7              C           D7       G
Ci-gît au fond de mon coeur une histoire ancienne,
       G7            C        D7        G
Un fantôme, un souvenir d'une que j'aimais...
          G7                 C           D7       G
Le temps, à grands coups de faux, peut faire des siennes,
         G7           C    D7            G
Mon bel amour dure encore, et c'est à jamais...


G                 Am             D7        G
J'ai perdu la Tramontane en trouvant Margot,
       Em          Am    D7           G
Princesse vêtue de laine, Déesse en sabots...
         Em                Am            D7          G
Si les fleurs, le long des routes, s'mettaient à marcher,
        Em               Am          D7            G
C'est à la Margot, sans doute, qu'elles feraient songer...


          Em            Bm      Em          Bm
J'lui ai dit: « De la Madone, tu es le portrait ! »
       Em            Bm             A7       D
Le Bon Dieu me le pardonne, c'était un peu vrai...
         G              Am               D7       G
Qu'il me le pardonne ou non, d'ailleurs, je m'en fous,
             Em       Am     D7          G
J'ai déjà mon âme en peine : je suis un voyou.


G                 Am             D7        G
La mignonne allait aux vêpres, se mettre à genoux,
       Em             Am       D7              G
Alors j'ai mordu ses lèvres, pour savoir leur goût...
         Em                Am                   D7      G
Elle m'a dit, d'un ton sévère : « qu'est-ce que tu fais là ? »
       Em              Am        D7                 G
Mais elle m'a laissé faire, les filles, c'est comme ça...


          Em            Bm      Em          Bm
J'lui ai dit: « Par la Madone, reste auprès de moi ! »
       Em            Bm              A7      D
Le Bon Dieu me le pardonne, mais chacun pour soi...
         G              Am               D7       G
Qu'il me le pardonne ou non, d'ailleurs, je m'en fous,
             Em       Am     D7          G
J'ai déjà mon âme en peine : je suis un voyou.


G                  Am             D7         G
C'était une fille sage, a « bouche, que veux-tu ? »
       Em                Am    D7           G
J'ai croqué dans son corsage, les fruits défendus...
         Em                Am                   D7      G
Elle m'a dit d'un ton sévère : « qu'est-ce que tu fais là ? »
        Em             Am               D7          G
Mais elle m'a laissé faire, les filles, c'est comme ça...


           Em          Bm        Em          Bm
Puis, j'ai déchiRé sa robe, sans l'avoir voulu...
       Em           Bm             A7      D
Le Bon Dieu me le pardonne, je n'y tenais plus !
         G              Am               D7       G
Qu'il me le pardonne ou non, d'ailleurs, je m'en fous,
       Em              Am     D7          G
J'ai déjà mon âme en peine : je suis un voyou.


G                    Am           D7     G
J'ai perdu la Tramontane, En perdant Margot,
       Em              Am   D7           G
Qui épousa, contre son âme, Un triste bigot...
         Em        Am          D7           G
Elle doit avoir à l'heure, A l'heure qu'il est,
         Em                Am            D7          G
Deux ou trois marmots qui pleurent, Pour avoir leur lait...


          Em             Bm       Em          Bm
Et, moi, j'ai tété leur mère, longtemps avant eux...
       Em           Bm             A7      D
Le Bon Dieu me le pardonne, j'étais amoureux !
         G              Am               D7       G
Qu'il me le pardonne ou non, d'ailleurs, je m'en fous,
       Em              Am     D7          G
J'ai déjà mon âme en peine : je suis un voyou.
```

------------
=> ../ Retour
=> gwit://self Accueil
